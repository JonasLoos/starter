from enum import Enum, unique, auto


@unique
class DisplayType(Enum):
	App = auto()
	Text = auto()
	Info = auto()

class SearchResultEntry:
	# main attributes
	image = None
	text = None
	provider = None
	start = None
	displayType = None
	desc = None
	# optinal attributes
	desc = None

	def __init__(self, provider, text, start, displayType, image=None, desc=None):
		self.provider = provider
		self.text = text
		self.image = image
		self.start = start
		self.displayType = displayType
		self.desc = desc
