"""helper functions"""
import sys

def eprint(text, *args):
    print("> "+text.format(*args), file=sys.stderr)
