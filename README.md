# Linux App Starter

A fast and simple linux app starter. Based on GTK and uses the .desktop files on the system to create it's entries.

![](https://jloos.de/images/starter.png)


## TODO

* load icon from path
* more info prividers
* run in background
* clean up apps.py
* async loading
* testing
* config
  * options how to handle multiple input words
* set result-list highlight to 2. if it receives focus from search bar (using arrow down) 
