#!/usr/bin/env python3

import apps # search installed apps

import cairo
import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk
from gi.repository import Gdk


class TransparentWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)

        # set window size and position
        # geom = self.get_display().get_monitor(0).get_geometry()
        # self.set_size_request(geom.width, geom.height-70) # TODO
        # self.move(0,0)
        self.set_size_request(800, 400)
        self.set_position(Gtk.WindowPosition.CENTER)

        # content
        mainbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.init_result_list(mainbox)
        self.init_search_bar(mainbox) # depends on result list init

        self.add(mainbox)


        # quit when window looses focus
        self.connect('focus-out-event', Gtk.main_quit)

        # magic. Has something to do with window transparenty
        self.connect('destroy', Gtk.main_quit)
        self.connect('draw', self.draw)

        screen = self.get_screen()
        visual = screen.get_rgba_visual()
        if visual and screen.is_composited():
            self.set_visual(visual)

        self.set_app_paintable(True)
        self.show_all()
        
        # style
        self.get_window().set_decorations(Gdk.WMDecoration.BORDER)
        # self.set_decorated(False)
        self.set_icon_name("search") # TODO disable
        self.init_css()

        # keyboard shortcuts
        self.connect("key-press-event", self.key_press)


    def init_search_bar(self, parent):
        """init main search bar with auto input detection"""
        search_bar = Gtk.SearchEntry(name="search-entry")
        search_bar.set_alignment(0.5)
        search_bar.connect("stop-search",Gtk.main_quit) # quit on excape
        self.apps = list(filter(lambda app: app.get("NoDisplay") != "true", apps.getApps()))

        # input -> new search
        def executeSearch(widget):
            rl = self.result_list
            for child in rl.get_children():
                rl.remove(child)
            rl.apps = apps.searchApps(widget.get_text(), self.apps)
            shortcuts = (["Enter","Alt-1","Alt-2","Alt-3","Alt-4","Alt-5","Alt-6","Alt-7","Alt-8","Alt-9"]+[" "]*(len(rl.apps)-10))[:len(rl.apps)][::-1]
            for app, shortcut in zip(rl.apps[::-1], shortcuts):
                box = Gtk.Box() # TODO: center text by fixing size of stuff around
                box.pack_start(Gtk.Image.new_from_icon_name(app[0].get("Icon"), 0), False, True, 0)
                name_label = Gtk.Label(app[0].get("Name"))
                name_label.get_style_context().add_class("name-label")
                box.pack_start(name_label, True, True, 0) # TODO
                shortcut_label = Gtk.Label(shortcut)
                shortcut_label.get_style_context().add_class("shortcut-label")
                box.pack_start(shortcut_label, False, True, 0) # TODO
                rl.insert(box, 0)
            rl.show_all()
            rl.select_row(rl.get_row_at_index(0))
        search_bar.connect("search_changed",executeSearch)

        # enter -> search and start first entry
        def executeSearchAndStart(widget):
            executeSearch(widget) # TODO
            self.startApp(self.result_list.apps[0][0])
        search_bar.connect("activate",executeSearchAndStart)
        
        parent.pack_start(search_bar, False, True, 0) # obj, expand, fill, padding

    def init_result_list(self, parent):
        """init scrollable result list"""
        listbox = Gtk.ListBox(name="listbox")
        listbox.set_activate_on_single_click(True) # instant activation
        listbox.set_selection_mode(Gtk.SelectionMode.BROWSE) # select only one
        # listbox.set_opacity(0.5)

        def row_activated_handler(listbox, row):
            self.startApp(listbox.apps[row.get_index()][0])
        listbox.connect("row_activated", row_activated_handler)

        listbox.apps = []
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.add(listbox)

        self.result_list = listbox
        parent.pack_end(scrolled_window, True, True, 0)


    def init_css(self):
        """load css from gui.css"""
        style_provider = Gtk.CssProvider()
        style_provider.load_from_path("gui.css")
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    def startApp(self, app):
        """start the given app and quit"""
        app.start()
        Gtk.main_quit()

    def draw(self, widget, context):
        """magic. Has something to do with window transparenty"""
        context.set_source_rgba(0, 0, 0, 0.5)
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.set_operator(cairo.OPERATOR_OVER)

    def key_press(self, window, event):
        """start n-th app if alt+n is pressed"""
        keyname = Gdk.keyval_name(event.keyval)
        if not event.state & Gdk.ModifierType.MOD1_MASK:
            return
        i = int(keyname) # TODO error handling
        if i:
            self.startApp(self.result_list.apps[i][0])

# start
TransparentWindow()
Gtk.main()
