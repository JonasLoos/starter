from entry import SearchResultEntry, DisplayType
from myhelpers import eprint
import requests
import json
import webbrowser

class Provider:
	"""search provider class to get infos from wikipedia"""

	def __init__(self):
		"""init stuff"""

	@staticmethod
	def makeStart(link):
		def start():
			eprint("wiki: opening: {}", link)
			webbrowser.open(link)

		return start

	def search(self, term):
		"""search wikipedia"""
		if not term or len(term.split(" ")) > 1:
			return []
		
		data = dict()

		try:
			r = requests.get('https://de.wikipedia.org/api/rest_v1/page/summary/'+term)  #TODO: async

			if r.status_code != 200:
				raise "bad status code"

			data = json.loads(r.text)
		except Exception as e:
			eprint("wiki: request for {} failed: {}, status: {}", term, e, r.status_code)
			return []

		eprint("wiki: got result for {} :D", term)  #DEBUG
		return [SearchResultEntry("wiki", data["displaytitle"], self.makeStart("https://de.wikipedia.org/wiki/"+term), DisplayType.Info, desc=data["extract"])]
