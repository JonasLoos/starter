import os
from pathlib import Path
import operator
from subprocess import Popen
from entry import SearchResultEntry, DisplayType
from myhelpers import eprint


APP_PATHS = ["/usr/share/applications", str(Path.home())+"/.local/share/applications"]


# >>> main script <<<

class App:
	"""App entry class"""

	def __init__(self, path):
		self.path = path
		self.desktop_file_name = os.path.splitext(os.path.basename(path))[0]
		self.sections = {}
		self.parse_desktop_file()

	def get(self, attr, section="Desktop Entry"):
		return self.sections[section][attr] if section in self.sections and attr in self.sections[section] else ""

	def start(self):
		eprint("apps: starting {}: {}", self.get("Name"), self.get("Exec"))
		Popen(filter(lambda x: len(x) not in [0,2] or (len(x) >= 1 and x[0] != "%"), self.get("Exec").split(" ")))

	def search(self, s, section="Desktop Entry"):
		"""calculates how good the app matches the search term"""

		def match(a,b):
			a = a.lower()
			b = b.lower()
			if a == b: return 1.
			if b.startswith(a): return 0.8
			if a in b: return 0.3
			return 0
			# TODO: match to second word: 0.5, hm... is it really necessary?
		
		if len(s) == 0: return (0,0)
		
		# name match
		val = match(s, self.get("Name", section=section))
		if val > 0: return (2,val)

		# other
		def f(x): return 1. - 1. / 2**x
		val = 0
		# executable name
		val += match(s, self.get("Exec").split("/")[-1].split(" ")[0])*3
		# keywords
		val += f(sum(match(s,x) for x in self.get("Keywords", section=section).split(";")))*2
		# categories
		val += f(sum(match(s,x) for x in self.get("Categories", section=section).split(";")))
		# .desktop-file name
		val += match(s, self.desktop_file_name)
		# comment
		val += f(sum(match(s,x) for x in self.get("Comment", section=section).split(";")))*0.5
		if val > 0: return (1,val)

		# none
		return (0, 0)

	@staticmethod
	def is_app(path):
		"""check if the given path points to a valid desktop file"""

		# check if path is a file
		if not os.path.isfile(path):
			eprint("error: not a file: {}", path)
			return False

		with open(path) as f:
			# read lines (no empty lines and comments)
			lines = list(filter(lambda x: len(x) > 1 and x[0] != "#", f.readlines()))

		# check if it's a desktop entry
		if lines == None or lines[0] != "[Desktop Entry]\n":
			return False

		return True

	def parse_desktop_file(self):
		# read file
		with open(self.path) as f:
			# read lines (no empty lines and comments)
			lines = list(filter(lambda x: len(x) > 1 and x[0] != "#", f.readlines()))

		section = "Desktop Entry"
		self.sections[section] = {}
		# parse lines
		for line in lines[1:]:
			
			# check for new section
			if line[0] == "[" and line[-2:] == "]\n":
				section = line[1:-2]
				self.sections[section] = {}
				continue
			
			tmp = line.split("=")

			# check if its a valid line: ...=...
			if len(tmp) < 2:
				eprint("apps parse_desktop_file: not a valid line: {}", line[:-1])
				continue

			# add line contents
			self.sections[section][tmp[0]] = "=".join(tmp[1:])[:-1]


	def __str__(self):
		return "{} ({})".format(self.get("Name"), len(self.sections))


def searchAppsForWord(term, apps):
	"""return search result list: [(app, a, b), ...]"""
	return list(filter(
		lambda x: x[1] > 0, 
		[(app, *app.search(term)) for app in apps]
	))


def sortSearchResults(search_results):
	"""sorts a list of search results: [(app, a, b), ...]"""
	return sorted(
		sorted(
			search_results,
			key=operator.itemgetter(2)
		), 
		key=operator.itemgetter(1)
	)[::-1]


class Provider:
	"""search provider class to search for installed apps"""
	apps = []

	def __init__(self):
		"""load apps-data from APP_PATHS"""
		for path in APP_PATHS:
			for entry in os.listdir(path=path):
				# create Object
				fullpath = path+"/"+entry
				if App.is_app(fullpath):
					app = App(fullpath)
					print('entry', app)
					if app and app.get("NoDisplay") != "true":
						self.apps += [app]

	def search(self, term):
		"""searchs apps for each word in terms and returns sorted results: : [(app, a, b), ...]""" #TODO update
		matches = {}
		for word in term.split(" "):
			if word == "": continue # ignore double spaces
			for match in searchAppsForWord(word, self.apps):
				app = match[0]
				if app in matches:
					if match[1] > matches[app][0]: # better class
						matches[app] = (match[1], match[2])
					elif match[1] == matches[app][0]: # same class
						matches[app] = (matches[app][0], matches[app][1]+match[2])
				else:
					matches[app] = (match[1], match[2])

		search_results = sortSearchResults([(app, matches[app][0], matches[app][1]) for app in matches])
		return [SearchResultEntry("apps", app[0].get("Name"), app[0].start, DisplayType.App, image=app[0].get("Icon")) for app in search_results]
