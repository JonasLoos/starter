"""basic search provider module to demonstate the needed structure"""

from myhelpers import eprint
from entry import SearchResultEntry, DisplayType


def start():
	eprint("test: started")

class Provider:
	"""search provider class for demonstration purposes"""

	def __init__(self):
		"""init stuff"""
		eprint("test: init")


	def search(self, term):
		"""do some searching and return a list of SearchResultEntries"""
		eprint("test: search: {}", term)
		return [ SearchResultEntry("test", text, start, DisplayType.Text) for text in ["yolo", "swag"] ]
