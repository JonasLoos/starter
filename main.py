#!/usr/bin/env python3

import sys
import os
import gui
import importlib

SEARCH_PROVIDER_DIR = "searchProviders"


class Starter:
	"""main class"""

	# list of initialized search provider objects 
	searchProviders = []


	def __init__(self):
		"""load config and search providers"""

		# TODO: load config

		# load search providers from SEARCH_PROVIDER_DIR
		# self.searchProviders = [importlib.import_module("."+x[:-3],SEARCH_PROVIDER_DIR).Provider() for x in os.listdir(SEARCH_PROVIDER_DIR) if x.endswith(".py") and not x.startswith("__")]
		self.searchProviders = [importlib.import_module("."+x[:-1],SEARCH_PROVIDER_DIR).Provider() for x in open("searchProviders/__enabled_search_providers.txt").readlines()]


	def startGui(self):
		"""start a gui to ask for input and show results"""
		gui.TransparentWindow(self)


	def search(self, term):
		"""return a dict of search results from all search providers"""
		# return [x.search(term) for x in self.searchProviders]
		return [x for p in self.searchProviders for x in p.search(term)]


def main():

	argc = len(sys.argv)

	# GUI
	if argc < 2:
		s = Starter()
		s.startGui()
		exit()

	# command line
	# TODO

	print("USAGE: {}".format(sys.argv[0]))


if __name__ == "__main__": main()
